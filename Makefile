
notebook:
	docker-compose up -d official

open:
	open http://localhost:8888/

pull:
	docker pull jupyter/datascience-notebook

build:
	docker-compose build official

clean:
	docker system prune
